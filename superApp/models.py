from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models

from datetime import date

from django.utils.functional import cached_property


class EmployeeVotes(models.Model):
    employee = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='votes')
    vote_date = models.DateField()
    menu_voted = models.ForeignKey('Menu', on_delete=models.DO_NOTHING, null=True)


class Employee(User):
    class Meta:
        proxy = True

    @property
    def can_vote(self):
        if not EmployeeVotes.objects.filter(employee=self.id, vote_date=date.today()).exists():
            return True
        return False

    def __str__(self):
        return self.get_username()


class Menu(models.Model):
    dishes = ArrayField(models.CharField(max_length=200), blank=True)
    for_date = models.DateField(auto_now=True)


class Restaurant(models.Model):
    name = models.CharField(max_length=64)
    menus = models.ManyToManyField(Menu, through='RestaurantMenuVotes')

    @cached_property
    def current_day_menu(self):
        return self.menu_votes.filter(menu__for_date=date.today()).order_by('-votes').first()

    @cached_property
    def current_day_menus(self):
        return self.menus.filter(for_date=date.today())


class RestaurantMenuVotes(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.DO_NOTHING, related_name='menu_votes', related_query_name='menu_votes')
    restaurant = models.ForeignKey(Restaurant, on_delete=models.DO_NOTHING, related_name='menu_votes')
    votes = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return f'{self.restaurant.name =} - {self.menu.id =} - votes - {self.votes}'
