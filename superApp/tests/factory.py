import uuid

from django.contrib.auth import get_user_model

import factory
from faker import Factory

from superApp.models import Restaurant, Menu

fake = Factory.create()


# class UserFactory(factory.django.DjangoModelFactory):
#     name = factory.Sequence(lambda n: "user_%d" % n)
#     username = factory.Sequence(lambda n: "user_%d" % n)
#     email = factory.Sequence(lambda n: "user_%d@google.com" % n)
#     password = "my_password"
#
#     @classmethod
#     def _create(cls, model_class, *args, **kwargs):
#         """Override the default ``_create`` with our custom call."""
#         manager = cls._get_manager(model_class)
#         # The default would use ``manager.create(*args, **kwargs)``
#         return manager.create_user(*args, **kwargs)
#
#     class Meta:
#         model = get_user_model()


class RestaurantFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: "restaurant_%d" % n)

    class Meta:
        model = Restaurant


class MenuFactory(factory.django.DjangoModelFactory):
    dishes = factory.List([factory.Sequence(lambda n: f"dish_{uuid.uuid4()}") for n in range(3)])

    class Meta:
        model = Menu
