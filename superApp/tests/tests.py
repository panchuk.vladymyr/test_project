from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate

from superApp.models import RestaurantMenuVotes
from superApp.tests.factory import RestaurantFactory, MenuFactory
from rest_framework.test import APIRequestFactory
from superApp.views import RestaurantViewSet, VoteMenuViewSet

from superApp.v1.serializers import VoteMenuSerializerV1
from superApp.v2.serializers import VoteMenuSerializerV2
from superApp.v1.managers.restaurant_manager import RestaurantMenuManagerV1
from superApp.v2.managers.restaurant_manager import RestaurantMenuManagerV2


class RestaurantTests(APITestCase):
    def setUp(self):
        self.highest_vote = 777
        self.restaurant_1 = RestaurantFactory()
        self.restaurant_2 = RestaurantFactory()
        self.restaurant_3 = RestaurantFactory()

        self.menu_1 = MenuFactory()
        self.menu_2 = MenuFactory()
        self.menu_3 = MenuFactory()

        self.test_user = User(username='Test', password='test123', email='test@test.com')
        self.test_user.save()
        self.factory = APIRequestFactory()
        self.current_day_menu_highest_vote = RestaurantViewSet.as_view(actions={'get': 'current_day_menu_highest_vote'})
        self.vote_menu = VoteMenuViewSet.as_view()

    def test_get_highest_vote(self):
        RestaurantMenuVotes.objects.create(menu=self.menu_1, restaurant=self.restaurant_1, votes=77)
        RestaurantMenuVotes.objects.create(menu=self.menu_2, restaurant=self.restaurant_2, votes=self.highest_vote)

        request = self.factory.get('/api/restaurant/')
        self.test_user.refresh_from_db()
        force_authenticate(request, user=self.test_user)

        response = self.current_day_menu_highest_vote(request)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('votes', 0), self.highest_vote)

    def test_vote_v1(self):
        def get_serializer_class(self):
            return VoteMenuSerializerV1

        def get_manager_class(self):
            return RestaurantMenuManagerV1

        RestaurantMenuVotes.objects.create(menu=self.menu_1, restaurant=self.restaurant_1, votes=77)
        RestaurantMenuVotes.objects.create(menu=self.menu_2, restaurant=self.restaurant_2, votes=self.highest_vote)

        request = self.factory.post('/api/restaurant/vote-menu/', {"menu_votes":
            {
              "menu_id": self.menu_2.id,
              "vote": 1
            }
        }, format='json')
        self.test_user.refresh_from_db()
        force_authenticate(request, user=self.test_user)

        self.vote_menu.view_class.get_serializer_class = get_serializer_class
        self.vote_menu.view_class.get_manager_class = get_manager_class
        response = self.vote_menu(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(RestaurantMenuVotes.objects.filter(votes=self.highest_vote+1))

    def test_vote_v2(self):
        def get_serializer_class(self):
            return VoteMenuSerializerV2

        def get_manager_class(self):
            return RestaurantMenuManagerV2

        RestaurantMenuVotes.objects.create(menu=self.menu_1, restaurant=self.restaurant_1, votes=0)
        RestaurantMenuVotes.objects.create(menu=self.menu_2, restaurant=self.restaurant_2, votes=0)
        RestaurantMenuVotes.objects.create(menu=self.menu_3, restaurant=self.restaurant_3, votes=0)

        request = self.factory.post('/api/restaurant/vote-menu/', {"menu_votes":
        [
            {
                "menu_id": self.menu_1.id,
                "vote": 1
            },
            {
                "menu_id": self.menu_2.id,
                "vote": 2
            },
            {
                "menu_id": self.menu_3.id,
                "vote": 3
            }
        ]
        }, format='json')
        self.test_user.refresh_from_db()
        force_authenticate(request, user=self.test_user)
        self.vote_menu.view_class.get_serializer_class = get_serializer_class
        self.vote_menu.view_class.get_manager_class = get_manager_class
        response = self.vote_menu(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(RestaurantMenuVotes.objects.filter(menu=self.menu_1).first().votes, 1)
        self.assertEqual(RestaurantMenuVotes.objects.filter(menu=self.menu_2).first().votes, 2)
        self.assertEqual(RestaurantMenuVotes.objects.filter(menu=self.menu_3).first().votes, 3)

        request = self.factory.post('/api/restaurant/vote-menu/', {"menu_votes":
            [
                {
                    "menu_id": self.menu_1.id,
                    "vote": 1
                },
                {
                    "menu_id": self.menu_3.id,
                    "vote": 3
                },
                {
                    "menu_id": self.menu_2.id,
                    "vote": 4
                }
            ]
        }, format='json')

        response = self.vote_menu(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        request = self.factory.post('/api/restaurant/vote-menu/', {"menu_votes":
            [
                {
                    "menu_id": self.menu_1.id,
                    "vote": 1
                },
                {
                    "menu_id": self.menu_3.id,
                    "vote": 3
                },
                {
                    "menu_id": self.menu_2.id,
                    "vote": 2
                },
                {
                    "menu_id": self.menu_2.id,
                    "vote": 2
                }
            ]
        }, format='json')

        response = self.vote_menu(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

