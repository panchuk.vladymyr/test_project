from django.db import transaction
from rest_framework import serializers

from superApp.models import Menu, Restaurant


class MenuSerializer(serializers.ModelSerializer):
    dishes = serializers.ListField()

    class Meta:
        model = Menu
        fields = ('id', 'dishes',)


class RestaurantUploadMenuSerializer(serializers.ModelSerializer):
    dishes = serializers.ListField()
    restaurant_id = serializers.SerializerMethodField()

    class Meta:
        model = Restaurant
        fields = ('dishes', 'restaurant_id')

    def get_restaurant_id(self, obj):
        return self.context.get('restaurant_id')

    def create(self, validated_data):
        restaurant = Restaurant.objects.get(id=self.data.get('restaurant_id'))
        with transaction.atomic():
            dishes = validated_data.get('dishes', [])
            instance = Menu.objects.create(dishes=dishes)
            restaurant.menus.add(instance.id)
        return restaurant


class RestaurantCurrentDayMenuSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=64)
    menu = serializers.SerializerMethodField()
    votes = serializers.SerializerMethodField()

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'menu', 'votes')

    def get_menu(self, instance):
        if instance.current_day_menu:
            if instance.current_day_menu.votes == 0:
                return ['There is no voted menu yet']
            return instance.current_day_menu.menu.dishes
        return ['There is no uploaded menu for today']

    def get_votes(self, instance):
        if instance.current_day_menu:
            return instance.current_day_menu.votes
        return 0


class RestaurantSerializerV1(serializers.ModelSerializer):
    name = serializers.CharField(max_length=64)
    menus = serializers.SerializerMethodField()

    def get_menus(self, instance):
        return MenuSerializer(instance.current_day_menus, many=True).data

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'menus')


class VoteMenuItemsSerializerV1(serializers.Serializer):
    menu_id = serializers.IntegerField()
    vote = serializers.IntegerField(max_value=3)


class VoteMenuSerializerV1(serializers.Serializer):
    menu_votes = VoteMenuItemsSerializerV1()


class RestaurantCurrentDayMenusSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=64)
    menus = serializers.SerializerMethodField()

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'menus')

    def get_menus(self, instance):
        if instance.current_day_menus:
            return MenuSerializer(instance.current_day_menus, many=True).data
        return ['There is no uploaded menu for today']
