from typing import List

from superApp.models import RestaurantMenuVotes


class RestaurantMenuManagerV1:
    error_not_available_to_vote = f"Current restaurant dont have this menu available to vote"
    error_too_high_vote_rate = f"too high vote rate"

    def __init__(self, menu_data: dict):
        self.errors = []
        self.menu_data = menu_data
        self.restaurant_vote_data = None

    def _validate_max_vote(self, data):
        if self.menu_data['menu_votes']['vote'] != 1:
            self.errors.append(self.error_too_high_vote_rate)

    def validate_vote_data(self):
        self._validate_max_vote(self.restaurant_vote_data)

    def can_vote(self):
        menu_id = self.menu_data['menu_votes']['menu_id']
        vote_rate = self.menu_data['menu_votes']['vote']
        instance = RestaurantMenuVotes.objects.filter(menu_id=menu_id).first()
        if not instance:
            self.can_vote_checked = False
            self.errors.append(self.error_not_available_to_vote)
        self.restaurant_vote_data = {'instance': instance, 'vote': vote_rate}

        self.validate_vote_data()
        if not self.errors:
            self.can_vote_checked = True
            return True
        return False

    def vote_menu(self):
        # TODO prevent user from vote more than once
        assert hasattr(self, 'can_vote_checked'), 'Call can_vote() before vote_menu()' if not self.errors else f'{self.errors}'
        instance = self.restaurant_vote_data['instance']
        if self.restaurant_vote_data:
            instance.votes += self.restaurant_vote_data['vote']
            instance.save()
        else:
            self.errors.append(self.error_not_available_to_vote)
