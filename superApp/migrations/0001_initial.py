# Generated by Django 4.0.6 on 2022-11-14 09:15

from django.conf import settings
import django.contrib.auth.models
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dishes', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=200), blank=True, size=None)),
                ('for_date', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='RestaurantMenuVotes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('votes', models.PositiveSmallIntegerField(default=0)),
                ('menu', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='menu_votes', related_query_name='menu_votes', to='superApp.menu')),
                ('restaurant', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='menu_votes', to='superApp.restaurant')),
            ],
        ),
        migrations.AddField(
            model_name='restaurant',
            name='menus',
            field=models.ManyToManyField(through='superApp.RestaurantMenuVotes', to='superApp.menu'),
        ),
        migrations.CreateModel(
            name='EmployeeVotes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vote_date', models.DateField()),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='votes', to=settings.AUTH_USER_MODEL)),
                ('menu_voted', models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='superApp.menu')),
            ],
        ),
    ]
