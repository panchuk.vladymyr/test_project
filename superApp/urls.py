from django.urls import path
from rest_framework import routers

from superApp.views import RestaurantViewSet, VoteMenuViewSet


app_name = "dashboard"

router = routers.SimpleRouter()
router.register(r'restaurant', RestaurantViewSet)


urlpatterns = [
    path('restaurant/vote-menu/', VoteMenuViewSet.as_view(), name='vote-menu'),
]

urlpatterns += router.urls
