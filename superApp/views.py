import datetime

from django.db.models import Max
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.generics import (GenericAPIView, )
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from superApp.v1.managers.restaurant_manager import RestaurantMenuManagerV1
from .models import Restaurant, Menu, RestaurantMenuVotes

from rest_framework import mixins, status, versioning

from .v1.serializers import VoteMenuSerializerV1, RestaurantSerializerV1, \
    RestaurantUploadMenuSerializer, RestaurantCurrentDayMenuSerializer, MenuSerializer, \
    RestaurantCurrentDayMenusSerializer
from .v2.managers.restaurant_manager import RestaurantMenuManagerV2
from .v2.serializers import VoteMenuSerializerV2


class RestaurantViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    """
    -  post method - Creating restaurant
    -  action upload_menu - Uploading menu for restaurant (There should be a menu for each day)
    -  action current_day_menu - Getting results for current day
    -  action current_day_menus - Getting current day menus
    """

    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializerV1
    serializer_action_classes = {
        "upload_menu": RestaurantUploadMenuSerializer,
        "current_day_menu_highest_vote": RestaurantCurrentDayMenuSerializer,
        "current_day_menus": RestaurantCurrentDayMenusSerializer,
    }
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        return self.serializer_action_classes.get(self.action, self.serializer_class)

    def retrieve(self, request, *args, **kwargs):
        restaurants = Restaurant.objects.filter(menu_votes__menu__for_date=datetime.date.today()).distinct('id')
        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['post'], url_path='upload_menu')
    @swagger_auto_schema(request_body=RestaurantUploadMenuSerializer)
    def upload_menu(self, request, *args, **kwargs):
        restaurant = self.get_object()
        serializer = self.get_serializer_class()(data=request.data, context={"restaurant_id": restaurant.id})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'], url_path='current_day_menu_highest_vote')
    def current_day_menu_highest_vote(self, request, *args, **kwargs):
        """
        get current day manu with the highest vote rate
        """
        restaurants = Restaurant.objects.filter(menu_votes__menu__for_date=datetime.date.today())
        restaurant_with_max_vote = restaurants.annotate(max_vote=Max('menu_votes__votes')).order_by('-max_vote').first()
        serializer = self.get_serializer_class()(restaurant_with_max_vote)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, methods=['get'], url_path='current_day_menus')
    def current_day_menus(self, request, *args, **kwargs):
        """
        current day menus available for vote
        """
        restaurants = Restaurant.objects.filter(menu_votes__menu__for_date=datetime.date.today()).distinct('id')
        serializer = self.get_serializer(restaurants, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class VoteMenuViewSet(GenericAPIView):
    """
    Voting for restaurant menu
    Use header 'ACCEPT' with 'application/json; version=1.0'
    where:
    version=1.0 - api accepts one menu
    payload - {"menu_votes": {"menu_id": <int:id>, "vote": <int:id>}}
    version=2.0 - api accepts top three menus with respective points (1 to 3)
    payload - {
        "menu_votes": [
            {"menu_id":<int:id>, "vote":<int:id>},
            {"menu_id":<int:id>, "vote":<int:id>},
            {"menu_id":<int:id>, "vote":<int:id>}
        ]
    }
    """
    versioning_class = versioning.AcceptHeaderVersioning
    queryset = RestaurantMenuVotes.objects.all()
    serializer_class = VoteMenuSerializerV2

    serializer_classes_mapping = {
        "1.0": VoteMenuSerializerV1,
        "2.0": VoteMenuSerializerV2
    }
    managers_mapping = {
        "1.0": RestaurantMenuManagerV1,
        "2.0": RestaurantMenuManagerV2
    }

    def get_serializer_class(self):
        return self.serializer_classes_mapping.get(self.request.version, VoteMenuSerializerV2)

    def get_manager_class(self):
        return self.managers_mapping.get(self.request.version, RestaurantMenuManagerV2)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()(data=request.data)
        manager = self.get_manager_class()(request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        if manager.can_vote():
            manager.vote_menu()
            return Response(status=status.HTTP_200_OK)
        return Response(manager.errors, status=status.HTTP_400_BAD_REQUEST)


class MenuViewSet(ModelViewSet):
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer
