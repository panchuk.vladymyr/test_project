from rest_framework import serializers


class VoteMenuItemsSerializerV1(serializers.Serializer):
    menu_id = serializers.IntegerField()
    vote = serializers.IntegerField(max_value=3)


class VoteMenuSerializerV2(serializers.Serializer):
    menu_votes = VoteMenuItemsSerializerV1(many=True, required=False)