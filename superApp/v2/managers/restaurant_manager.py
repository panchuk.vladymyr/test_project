from typing import List

from superApp.models import RestaurantMenuVotes


class RestaurantMenuManagerV2:
    error_not_available_to_vote = f"Current restaurant dont have one of menus available to vote"
    error_too_much_votes = f"too much votes, try vote only for 3 menus"

    def __init__(self, menu_data: List[dict]):
        self.errors = []
        self.menu_data = menu_data
        self.restaurant_vote_data = []

    def _validate_max_menu_count(self, data):
        if len(data) > 3:
            self.errors.append(self.error_too_much_votes)

    def validate_vote_data(self):
        self._validate_max_menu_count(self.restaurant_vote_data)

    def can_vote(self):
        for menu_vote in self.menu_data['menu_votes']:
            menu_id = menu_vote.get('menu_id')
            vote_rate = menu_vote.get('vote')
            instance = RestaurantMenuVotes.objects.filter(menu_id=menu_id).first()
            self.restaurant_vote_data.append({'instance': instance, 'vote': vote_rate})
        self.validate_vote_data()
        if not self.errors:
            self.can_vote_checked = True
            return True
        return False

    def vote_menu(self):
        # TODO prevent user from vote more than once
        assert hasattr(self, 'can_vote_checked'), ('Call can_vote() before vote_menu()')

        for menu_vote in self.restaurant_vote_data:
            vote_rate = menu_vote.get('vote')
            instance = menu_vote.get('instance')
            if instance:
                instance.votes += vote_rate
                instance.save()
            else:
                self.errors.append(self.error_not_available_to_vote)
