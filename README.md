# test_project
Clone project  

run - docker compose up

use http://127.0.0.1:8000/swagger/ to see all endpoints

create employee {{host}}/auth/users/

create bearer token {{host}}/auth/jwt/create/ 

create restaurant {{host}}/api/restaurant/ 

upload restaurant menu {{host}}/api/restaurant/<id>/upload_menu/ 

see menus available for today {{host}}/api/restaurant/current_day_menus/

vote for menu {{host}}/api/restaurant/vote-menu/ 
to vote use "Accept" header with "application/json; version=1.0" or "application/json; version=2.0" depends what version of api you want to use 

see high voted menu {{host}}/api/restaurant/current_day_menu_highest_vote/  
